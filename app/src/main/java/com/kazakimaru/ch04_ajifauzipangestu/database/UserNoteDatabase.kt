package com.kazakimaru.ch04_ajifauzipangestu.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [User::class, Note::class], version = 1)
abstract class UserNoteDatabase: RoomDatabase() {
    abstract fun userNoteDao(): UserNoteDao

    companion object {
        private var INSTANCE : UserNoteDatabase? = null

        fun getInstance(context: Context): UserNoteDatabase? {
            if (INSTANCE == null) {
                synchronized(UserNoteDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext, UserNoteDatabase::class.java, "Catatan.db").build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}