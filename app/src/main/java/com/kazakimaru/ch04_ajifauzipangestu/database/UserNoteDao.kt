package com.kazakimaru.ch04_ajifauzipangestu.database

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE

@Dao
interface UserNoteDao {
    @Insert(onConflict = REPLACE)
    fun insertUser(user: User): Long

    @Query("SELECT * FROM User WHERE username = :username")
    fun checkRegisteredUsername(username: String): List<User>

    @Query("SELECT * FROM User WHERE username = :username AND password = :password")
    fun checkLogin(username: String, password: String): List<User>

    @Query("SELECT * FROM User WHERE username = :username")
    fun getUserId(username: String): User

    @Insert(onConflict = REPLACE)
    fun insertNote(note: Note): Long

    @Query("SELECT * FROM Note WHERE id_user = :userId")
    fun getAllNote(userId: Int): List<Note>

    @Update
    fun updateNote(note: Note): Int

    @Delete
    fun deleteNote(note: Note): Int

}