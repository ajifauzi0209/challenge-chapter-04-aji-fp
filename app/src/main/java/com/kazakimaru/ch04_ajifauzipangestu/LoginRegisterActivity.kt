package com.kazakimaru.ch04_ajifauzipangestu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.kazakimaru.ch04_ajifauzipangestu.databinding.ActivityLoginRegisterBinding

class LoginRegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginRegisterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginRegisterBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }
}